package com.adixsoft.pda;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;

import com.adixsoft.pda.Fragment.CalenderFragment;
import com.adixsoft.pda.Fragment.HomeFragment;
import com.adixsoft.pda.Fragment.NotificationFragment;
import com.adixsoft.pda.Fragment.ProfileFragment;
import com.adixsoft.pda.Fragment.SettingsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.bottom_navigation);


        bottomNavigationView.setOnNavigationItemSelectedListener(this);



    }




    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId() ) {

            case R.id.menu_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame, new HomeFragment()).addToBackStack(null).commit();
                break;

            case R.id.menu_calender:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame, new CalenderFragment()).addToBackStack(null).commit();

                //  Toast.makeText(MainActivity.this, "Nearby", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_notification:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame, new NotificationFragment()).addToBackStack(null).commit();
                break;
            case  R.id.menu_settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame, new SettingsFragment()).addToBackStack(null).commit();
                // Toast.makeText(MainActivity.this, "Favorites", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame, new ProfileFragment()).addToBackStack(null).commit();
                break;


        }
        return true;
    }
}
