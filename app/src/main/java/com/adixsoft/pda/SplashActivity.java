package com.adixsoft.pda;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.adixsoft.pda.Auth.LoginActivity;
import com.adixsoft.pda.Auth.SignUpActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnSignIn;
    TextView btnSignup;
    private FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mAuth = FirebaseAuth.getInstance();
        btnSignIn = findViewById(R.id.btn_login);
        btnSignup = findViewById(R.id.btn_signup);


       /* new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                // This method will be executed once the timer is over
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, 2000);*/

        FirebaseUser currentUser = mAuth.getCurrentUser();

        mAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser()==null)
                {
                    //startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                } else {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);

                }
            }
        };

      //  updateUI(currentUser);

      /*  if(currentUser!= null){
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
        } else {
            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(i);
        }*/



        btnSignIn.setOnClickListener(this);
        btnSignup.setOnClickListener(this);

    }
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListner);
    }



    private void updateUI(FirebaseUser currentUser) {
        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case  R.id.btn_login: {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                break;
            }

            case R.id.btn_signup: {
                Intent i = new Intent(SplashActivity.this, SignUpActivity.class);
                startActivity(i);
                break;
            }



        }

    }
}
