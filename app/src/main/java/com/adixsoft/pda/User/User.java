package com.adixsoft.pda.User;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

    public String name;
    public String email;
    public String phone;
    public String password;
    public String status;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public User() {
    }

    public User(String name, String email,String phone, String password,String status) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password= password;
        this.status = status;
    }
}