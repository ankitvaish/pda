package com.adixsoft.pda.Auth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.adixsoft.pda.MainActivity;
import com.adixsoft.pda.R;

import com.adixsoft.pda.User.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class SignUpActivity extends AppCompatActivity {

    TextView textSignin;
    private FirebaseAuth mAuth;
    Button btnSignup;
    TextView relationStatus;
    ProgressBar progressBar;
    Spinner spinner;
    ArrayAdapter adapter;
    EditText edit_email,edit_password,editName,editPhone;
    String[] country = { "Relationship Status", "Single", "Married"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mAuth = FirebaseAuth.getInstance();
        textSignin= findViewById(R.id.link_signin);
        edit_email = findViewById(R.id.edit_email);
        edit_password = findViewById(R.id.edit_pass);
        btnSignup =findViewById(R.id.btn_signup);
        relationStatus = findViewById(R.id.edit_relation_status);
        editName =findViewById(R.id.edit_name);
        editPhone =findViewById(R.id.edit_phone);
        progressBar = findViewById(R.id.progressBar);
       spinner = (Spinner) findViewById(R.id.planets_spinner);
         adapter = new ArrayAdapter<String>(this,
                 R.layout.spinner_textview_item, country);



        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setVisibility(View.VISIBLE);


       /* relationStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                adapter.setDropDownViewResource(R.layout.spinner_textview_item);
                spinner.setAdapter(adapter);
                spinner.setVisibility(View.VISIBLE);

            }
        });*/






        textSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i1 = new Intent(SignUpActivity.this,LoginActivity.class);
                startActivity(i1);
            }
        });


        relationStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {





            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {



            @Override
            public void onClick(View view) {
                makeSignup();

            }
        });



    }

    private void makeSignup() {


        final  String username = editName.getText().toString();
        final  String userphone = editPhone.getText().toString();
        final  String email = edit_email.getText().toString();
        final  String password = edit_password.getText().toString();



        if (TextUtils.isEmpty(username)) {
            Toast.makeText(getApplicationContext(), "Enter Username", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(userphone)) {
            Toast.makeText(getApplicationContext(), "Enter Phone number", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Enter Eamil Id", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Enter Password", Toast.LENGTH_SHORT).show();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);


        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "createUserWithEmail:success");
                            FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();

                            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");

                           String userId = current_user.getUid();
                          //  String userId = mDatabase.push().getKey();

                            User user = new User(username, email,userphone,password,"single");

                            mDatabase.child(userId).setValue(user);







                            Intent i = new Intent(SignUpActivity.this, MainActivity.class);
                            startActivity(i);



                        } else {
                            progressBar.setVisibility(View.GONE);
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUpActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                           // updateUI(null);
                        }


                    }
                });


    }
}
